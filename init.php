<?php
/**
 * Plugin Name: Limit Login Attempts on Login only
 * Description: Trigger the Limit Login Attempts plugin only on login
 * Plugin URI: https://gitlab.com/walterebert/limit-login-attempts-on-login-only
 * Author: Walter Ebert
 * Author URI: http://walterebert.com/
 * Version: 1.2.1
 * License: GPLv2 or later
 * License URI: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * @package LimitLoginAttempts
 */

/**
 * Limit login attempts on login only
 */
function limit_login_attempts_on_login_only() {
	$priority = 99999;
	if ( defined( 'LIMIT_LOGIN_ATTEMPTS_ON_LOGIN_ONLY_PRIORITY' ) && LIMIT_LOGIN_ATTEMPTS_ON_LOGIN_ONLY_PRIORITY > 0 ) {
		$priority = absint( LIMIT_LOGIN_ATTEMPTS_ON_LOGIN_ONLY_PRIORITY );
	}

	$request_uri = filter_input( INPUT_SERVER, 'REQUEST_URI', FILTER_SANITIZE_URL );

	if ( ! is_admin() && $request_uri && ! preg_match( '#' . parse_url( wp_login_url(), PHP_URL_PATH ) . '$#', $request_uri ) ) {
		remove_action( 'plugins_loaded', 'limit_login_setup', $priority );
	}
}
add_action( 'plugins_loaded', 'limit_login_attempts_on_login_only' );
