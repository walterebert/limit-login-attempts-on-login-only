# Limit login attempts on login only
Trigger the [Limit Login Attempts](https://wordpress.org/plugins/limit-login-attempts/) WordPress plugin only on login
